﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using Microsoft.AspNet.SignalR;
using SettlersOfCatan.GameOfMemory;
using SettlersOfCatan.GameOfMemory.Models;

// TODO: Log chat and display when logging in
namespace SettlersOfCatan.Hubs
{
    public class PerfHub : Hub // TODO: ,ISend. IJoin, IListAvailableGames etc
    {
        static List<SettlersBoard> _boardGames = new List<SettlersBoard>
        {
            new SettlersBoard
            {
                Room = "3 bots",
            }
        };
        static List<string> _chatMessageses = new List<string>();

        public class RoomList : IEquatable<RoomList>
        #region Rooms
        {
            public string Room { get; set; }
            public string ConnectionId { get; set; }
            public string UserName { get; set; }
            public bool ReadyToStartTheGame { get; set; } = false;

            public bool Equals(RoomList other)
            {
                return Room.Equals(other.Room)
                    && UserName.Equals(other.UserName);
            }
        }
        #endregion

        static HashSet<RoomList> ConnectedUsers = new HashSet<RoomList>
        {
            new RoomList
            {
                ConnectionId = "1",
                    UserName = "bot1",
                    Room = "1 bot",
                    ReadyToStartTheGame = true,
            },
            new RoomList
                {
                    ConnectionId = "1",
                    UserName = "bot1",
                    Room = "full room",
                    ReadyToStartTheGame = true,
                },
            new RoomList
                {
                    ConnectionId = "2",
                    UserName = "bot2",
                    Room = "full room",
                    ReadyToStartTheGame = true,
                },
            new RoomList
                {
                    ConnectionId = "3",
                    UserName = "bot3",
                    Room = "full room",
                    ReadyToStartTheGame = true,
                },
            new RoomList
                {
                    ConnectionId = "4",
                    UserName = "bot4",
                    Room = "full room",
                    ReadyToStartTheGame = true,
                },
            new RoomList
                {
                    ConnectionId = "2",
                    UserName = "bot1",
                    Room = "2 bots",
                    ReadyToStartTheGame = true,
                },
            new RoomList //
                {
                    ConnectionId = "3",
                    UserName = "bot2",
                    Room = "2 bots",
                    ReadyToStartTheGame = true,
                }

        };

        // list of functions performed so far for relogged users
        readonly List<ServerInstructions> notifications = new List<ServerInstructions>();

        public void Send(string id, string message, string game)
        // TODO: Add exclude to rooms with users!!!
        #region Demo & Start The game Logic
        {
            if (!String.IsNullOrEmpty(id))
            {
                if (id == "1")
                {
                    notifications.Add(new ServerInstructions { function = "show", element = ".catan-road--container" });
                    notifications.Add(new ServerInstructions { function = "show", element = ".catan-house--container" });
                    Clients.Caller.sendInstructions("serverFunction", notifications);
                }
                else if (id == "2")
                {
                    notifications.Add(new ServerInstructions { function = "hide", element = ".catan-road--container" });
                    notifications.Add(new ServerInstructions { function = "hide", element = ".catan-house--container" });
                    Clients.Caller.sendInstructions("serverFunction", notifications);
                }
                else if (id == "3")
                {
                    notifications.Add(new ServerInstructions { function = "show", element = ".catan-shore--ship" });
                    Clients.All.sendInstructions("serverFunction", notifications);
                }
                else if (id == "4")
                {
                    notifications.Add(new ServerInstructions { function = "hide", element = ".catan-shore--ship" });
                    Clients.All.sendInstructions("serverFunction", notifications);
                }
                else if (id == "5")
                {
                    notifications.Add(new ServerInstructions { function = "show", element = ".game-tile" });
                    Clients.All.sendInstructions("serverFunction", notifications);
                }
                else if (id == "6")
                {
                    notifications.Add(new ServerInstructions { function = "hide", element = ".game-tile" });
                    Clients.All.sendInstructions("serverFunction", notifications);
                }
                else if (id == "7")
                {
                    notifications.Add(new ServerInstructions { function = "show", element = ".catan-road--container" });
                    notifications.Add(new ServerInstructions { function = "show", element = ".catan-shore--ship" });
                    notifications.Add(new ServerInstructions { function = "show", element = ".game-tile" });
                    notifications.Add(new ServerInstructions { function = "show", element = ".catan-house--container" });
                    Clients.All.sendInstructions("serverFunction", notifications);
                }
                else if (id == "8")
                {
                    notifications.Add(new ServerInstructions { function = "hide", element = ".catan-road--container" });
                    notifications.Add(new ServerInstructions { function = "hide", element = ".catan-shore--ship" });
                    notifications.Add(new ServerInstructions { function = "hide", element = ".game-tile" });
                    notifications.Add(new ServerInstructions { function = "hide", element = ".catan-house--container" });
                    Clients.All.sendInstructions("serverFunction", notifications);
                }

                #endregion
                else if (id == "start-the-game")
                {
                    // TODO: CHECK IF THE GAMEBOARD EXIST, if yes, buildgameboard!!! 

                    notifications.Add(new ServerInstructions { function = "disable_start_button", element = "" });
                    Clients.Caller.sendInstructions("serverFunction", notifications);
                    notifications.Clear();

                    SendNotification("green", "caller", "<b>" + ConnectedUsers.Count(p => p.Room == game) + "</b>" + " users are connected to the game!");

                    var user = Context.User.Identity.Name;
                    user = user.Substring(0, user.IndexOf("@"));
                    var room = String.Empty;

                    foreach (var member in ConnectedUsers.Where(r => r.Room == game))
                    {
                        if (member.UserName == Context.User.Identity.Name)
                        {
                            room = member.Room;
                            member.ReadyToStartTheGame = true;
                            notifications.Add(new ServerInstructions
                            {
                                function = "notification_green",
                                element = "<b>" + user + "</b>"
                                + " is ready to start the game and number of people ready to play the game is: "
                                + ConnectedUsers.Count(r => r.Room == room && r.ReadyToStartTheGame) + " out of "
                                + ConnectedUsers.Count(p => p.Room == game)
                            });
                            Clients.Group(game).sendInstructions("serverFunction", notifications);
                            notifications.Clear();

                            if (ConnectedUsers.Count(p => p.Room == game) == ConnectedUsers.Count(r => r.Room == room && r.ReadyToStartTheGame)
                                && ConnectedUsers.Count(p => p.Room == game) > 1) // at least 2 players in a game
                            {
                                buildSettlersboard(room);
                            }
                        }
                    }

                }
                else
                {
                    notifications.Add(new ServerInstructions { function = id, element = "temp" });
                    // Call the addNewMessageToPage method to update clients.
                    Clients.All.sendInstructions(id, notifications);
                    //Clients.All.addNewMessageToPage(id, message); 
                }
            }
        }

        private void SendNotification(string color, string recipient, string message)
        {
            notifications.Add(new ServerInstructions { function = "notification_" + color, element = message });
            switch (recipient)
            {
                case "caller":
                    Clients.Caller.sendInstructions("serverFunction", notifications);
                    break;
                default:
                    Clients.Group(recipient).sendInstructions("serverFunction", notifications);
                    break;
            }

            notifications.Clear();

        }

        private void buildSettlersboard(string gameID)
        {
            // if there's an existing board
            if (_boardGames.Any(r => r.Room == gameID))
            {
                SendNotification("red", "caller", "<b>You have joined an existing game!</b>");
                notifications.Add(new ServerInstructions { function = "show", element = ".game-tile" });
                notifications.Add(new ServerInstructions { function = "show", element = ".catan-shore--ship" });
                Clients.Caller.sendInstructions("serverFunction", notifications);
                Clients.Caller.StartGame(_boardGames.Where(r => r.Room == gameID));

                // if it's callers turn, take a turn
                if (_boardGames.FirstOrDefault(r => r.Room == gameID).WhosTurn == Context.User.Identity.Name)
                {
                    NewPlayerTurn(_boardGames.FirstOrDefault(r => r.Room == gameID).WhosTurn);

                    if (true) // if there are less than 1 houses and roads for each (ACTUAL!!!) opponent, then 
                    {
                        buildhouseandroad(_boardGames.FirstOrDefault(r => r.Room == gameID).WhosTurn);
                    }
                }
                return;
            }



            // -------------------
            var player1 = String.Empty;
            var player2 = String.Empty;
            var player3 = String.Empty;
            var player4 = String.Empty;

            // todo: if someone joins later, let them spectate!
            foreach (var member in ConnectedUsers.Where(r => r.Room == gameID))
            {
                if (player1 == "")
                {
                    player1 = member.UserName;
                }
                else if (player2 == "")
                {
                    player2 = member.UserName;
                }
                else if (player3 == "")
                {
                    player3 = member.UserName;
                }
                else if (player4 == "")
                {
                    player4 = member.UserName;
                }
            }

            var playingmembers = new List<string>();
            foreach (var member in ConnectedUsers
                .Where(r => (r.UserName != "" && r.UserName != "bot1" && r.UserName != "bot2" && r.UserName != "bot3" && r.UserName != "bot4") && r.Room == gameID))
            {
                playingmembers.Add(member.UserName);
            }

            var whosTurn = playingmembers[new Random().Next(0, playingmembers.Count)];
            // todo: convert to list and remove empty array!
            //string[] players = { player1, player2, player3, player4 };
            //var WhosTurn = players[new Random().Next(0, players.Length)];
            _boardGames.Add(new SettlersBoard
            {
                Player1 = player1,
                Player2 = player2,
                Player3 = player3,
                Player4 = player4,
                Room = gameID,
                WhosTurn = whosTurn
            });

            SendNotification("red", gameID, "<b>Game Started!</b>");
            notifications.Add(new ServerInstructions { function = "show", element = ".game-tile" });
            notifications.Add(new ServerInstructions { function = "show", element = ".catan-shore--ship" });
            Clients.Group(gameID).sendInstructions("serverFunction", notifications);
            Clients.Group(gameID).StartGame(_boardGames.Where(r => r.Room == gameID));
            NewPlayerTurn(whosTurn);

            if (true) // if there are less than 1 houses and roads for each (ACTUAL!!!) opponent, then 
            {
                buildhouseandroad(_boardGames.FirstOrDefault(r => r.Room == gameID).WhosTurn);
            }
        }

        private void NewPlayerTurn(string whosTurn)
        {
            Clients.User(whosTurn).StartNewTurn();
        }

        // Chat
        public void ListChat() //
        {
            foreach (var msg in _chatMessageses)
            {
                Clients.Caller.newMessage(msg);
            }
        }

        // todo: add clear stored messages!
        public void Send(string message)
        #region ChatMessages
        {
            if (message != "")
            {
                var user = Context.User.Identity.Name;
                user = user.Substring(0, user.IndexOf("@"));
                Clients.All.newMessage("<i>" + user + "</i>" + ": <b>" + message + "</b>");
                _chatMessageses.Add("<i>" + user + "</i>" + ": <b>" + message + "</b>");
            }
        }
        #endregion

        private bool userExistInARoom(string user, string room)
        {
            return ConnectedUsers.Any(p => p.UserName == user && p.Room == room);
        }

        public void JoinGroup(string groupName)
        {

            var user = Context.User.Identity.Name;
            List<ServerInstructions> notifications = new List<ServerInstructions>();
            if (groupName == "")
            {
                notifications.Add(new ServerInstructions { function = "notification_red", element = "<b> " + "THE GAME ROOM NAME CANNOT BE BLANK!!!" + " </b>" });
                //Clients.All.sendInstructions("serverFunction", notifications);
                Clients.Caller.sendInstructions("serverFunction", notifications);
            }
            else
            {
                // there room is full and does not contain the calling user
                if (ConnectedUsers.Count(p => p.Room == groupName) == 4 && !ConnectedUsers.Any(p => p.UserName == Context.User.Identity.Name && p.Room == groupName))
                {
                    notifications.Add(new ServerInstructions { function = "notification_red", element = "<b>" + "Room IS FULL!!! </b>" });
                    Clients.Caller.sendInstructions("serverFunction", notifications);
                    return;
                }

                // if the user is not in a room, add him
                // and update a list for everyone!
                if (!userExistInARoom(user, groupName))
                {
                    ConnectedUsers.Add(new RoomList
                    {
                        ConnectionId = Context.ConnectionId,
                        UserName = Context.User.Identity.Name,
                        Room = groupName
                    });
                    ListAvailableGames("all");
                }
            }


            user = user.Substring(0, user.IndexOf("@"));
            Groups.Add(Context.ConnectionId, groupName);
            notifications.Add(new ServerInstructions { function = "notification_green", element = "<b>" + user.ToUpper() + "</b>" + " has joined a room: " + "<b>" + groupName + "</b>" });

            notifications.Add(new ServerInstructions { function = "delete_item", element = "#modal-popup-container" });
            notifications.Add(new ServerInstructions { function = "delete_item", element = "#show-hide-demo-tab-closed" });
            notifications.Add(new ServerInstructions { function = "delete_item", element = "#start-the-game" });
            notifications.Add(new ServerInstructions { function = "delete_item", element = "#start-the-game-text-box" });
            notifications.Add(new ServerInstructions { function = "delete_item", element = "#list-available-games" });
            notifications.Add(new ServerInstructions { function = "delete_item", element = "#available-games" });
            notifications.Add(new ServerInstructions { function = "hide", element = ".catan-road--container" });
            notifications.Add(new ServerInstructions { function = "hide", element = ".catan-shore--ship" });
            notifications.Add(new ServerInstructions { function = "hide", element = ".game-tile" });
            notifications.Add(new ServerInstructions { function = "hide", element = ".catan-house--container" });
            notifications.Add(new ServerInstructions { function = "show", element = "#start-the-game" });
            notifications.Add(new ServerInstructions { function = "assign_game_variable", element = groupName });

            Clients.Caller.sendInstructions("serverFunction", notifications);

            if (_boardGames.Any(r => r.Room == groupName))
            {
                buildSettlersboard(groupName);
            }
        }

        //private void buildExistingSettlersboard(string gameID)
        //{
        //    if (_boardGames.Any(r => r.Room == gameID))
        //    {
        //        SendNotification("red", "caller", "<b>You have joined an existing game!</b>");
        //        notifications.Add(new ServerInstructions { function = "show", element = ".game-tile" });
        //        notifications.Add(new ServerInstructions { function = "show", element = ".catan-shore--ship" });
        //        Clients.Caller.sendInstructions("serverFunction", notifications);
        //        Clients.Caller.StartGame(_boardGames.Where(r => r.Room == gameID));

        //        // if it's callers turn, take a turn
        //        if (_boardGames.FirstOrDefault(r => r.Room == gameID).WhosTurn == Context.User.Identity.Name)
        //        {
        //            NewPlayerTurn(_boardGames.FirstOrDefault(r => r.Room == gameID).WhosTurn);

        //            if (true) // if there are less than 1 houses and roads for each opponent, then 
        //            {
        //                buildhouseandroad(_boardGames.FirstOrDefault(r => r.Room == gameID).WhosTurn);
        //            }
        //        }
        //    }

        //    return;
        //}

        private void buildhouseandroad(string whosTurn)
        {
            // model for title, message, buttons!
            var message = new NewTurnInstructions
            {
                title = "New Title from server",
                message = "New message from server!",
                buttons = new List<Buttons>
                {
                    new Buttons
                    {
                        cssClass = "first",
                        label = "Close1",
                        type = "close"
                    },
                    new Buttons
                    {
                        cssClass = "second",
                        label = "Close2",
                    },
                    new Buttons
                    {
                        cssClass = "third",
                        label = "Close3",
                    },
                    new Buttons
                    {
                        cssClass = "fourth",
                        label = "Close4",
                    },
                    new Buttons
                    {
                        cssClass = "firth",
                        label = "Close5",
                    },
                }
            };

            notifications.Add(new ServerInstructions { function = "show", element = ".catan-road--container" });
            notifications.Add(new ServerInstructions { function = "show", element = ".catan-house--container" });
            Clients.User(whosTurn).sendInstructions("serverFunction", notifications);
            notifications.Clear();
            Clients.User(whosTurn).BuildHouseAndRoad(message);
        }

        public void ListAvailableGames(string users)
        {
            List<RoomList> SortedRoomsUsers = ConnectedUsers.OrderBy(o => o.Room).ToList();
            string message = String.Empty;
            string room = String.Empty;
            string user = String.Empty;
            foreach (var var in SortedRoomsUsers)
            {
                user = var.UserName;
                if (user.Contains("@"))
                {
                    user = user.Substring(0, user.IndexOf("@"));
                }
                if (room != var.Room)
                {
                    room = var.Room;
                    message += "room," + room + "," + user + ",";
                }
                else
                {
                    message += user + ",";
                }
            }
            message = message.Trim(',');

            //notifications.Add(new ServerInstructions { function = "notification_orange", element = message });
            notifications.Add(new ServerInstructions { function = "list_rooms", element = message });
            if (users == "all")
            {
                Clients.All.sendInstructions("serverFunction", notifications);
            }
            else
            {
                Clients.Caller.sendInstructions("serverFunction", notifications);
            }
        }


        public void ConfirmHouseRoadSelection(string house, string road)
        {
            // todo: check that house/road is not already taken!!!
            var room = findPlayerGameRoom();
            _boardGames.FirstOrDefault(g => g.Room == room).House.Add(new House
            {
                Id = house,
                Owner = Context.User.Identity.Name,
            });
            _boardGames.FirstOrDefault(g => g.Room == room).Road.Add(new Road
            {
                Id = road,
                Owner = Context.User.Identity.Name,
            });
            UpdatePlayersInAGame(room);

            // todo: if #of houses < 3, then next turn!
        }

        private void UpdatePlayersInAGame(string gameID)
        {
            Clients.Group(gameID).StartGame(_boardGames.Where(r => r.Room == gameID));
            SendNotification("green", gameID, "Updating all views for the game: " + gameID + "!!! :)");
        }

        private string findPlayerGameRoom()
        {
            var user = Context.User.Identity.Name;
            return _boardGames.FirstOrDefault(b => b.Player1 == user || b.Player2 == user || b.Player3 == user || b.Player4 == user).Room;
        }


        public bool Join(string userName)
        {
            var player = GameState.Instance.GetPlayer(userName);
            if (player != null)
            {
                Clients.Caller.playerExists();
                return true;
            }

            player = GameState.Instance.CreatePlayer(userName);
            player.ConnectionId = Context.ConnectionId;
            Clients.Caller.name = player.Name;
            Clients.Caller.hash = player.Hash;
            Clients.Caller.id = player.Id;

            Clients.Caller.playerJoined(player); //

            return StartGame(player);
        }

        private bool StartGame(Player player)
        {
            if (player != null)
            {
                Player player2;
                var game = GameState.Instance.FindGame(player, out player2);
                if (game != null)
                {
                    Clients.Group(player.Group).buildBoard(game);
                    return true;
                }

                player2 = GameState.Instance.GetNewOpponent(player);
                if (player2 == null)
                {
                    Clients.Caller.waitingList();
                    return true;
                }

                game = GameState.Instance.CreateGame(player, player2);
                game.WhosTurn = player.Id;

                Clients.Group(player.Group).buildBoard(game);
                return true;
            }
            return false;
        }

        public bool JoinSettlersGame(string userName)
        {
            var player = GameState.Instance.GetPlayer(userName);
            if (player != null)
            {
                Clients.Caller.playerExists();
                return true;
            }

            player = GameState.Instance.CreatePlayer(userName);
            player.ConnectionId = Context.ConnectionId;
            Clients.Caller.name = player.Name;
            Clients.Caller.hash = player.Hash;
            Clients.Caller.id = player.Id;

            Clients.Caller.playerJoined(player);

            return StartSettlersGame(player);
        }

        private bool StartSettlersGame(Player player)
        {
            if (player != null)
            {
                Player player2;
                Player player3;
                Player player4;
                var game = GameState.Instance.FindSettlersGame(player, out player2, out player3, out player4);
                if (game != null)
                {
                    Clients.Group(player.Group).buildBoard(game);
                    return true;
                }

                player2 = GameState.Instance.GetNewOpponent(player);
                if (player2 == null || player3 == null || player4 == null)
                {
                    Clients.Caller.waitingList();
                    return true;
                }

                game = GameState.Instance.CreateSettlersGame(player, player2, player3, player4);
                game.WhosTurn = player.Id;

                Clients.Group(player.Group).buildBoard(game);
                return true;
            }
            return false;
        }

        public bool Flip(string cardName)
        {
            var userName = Clients.Caller.name;
            var player = GameState.Instance.GetPlayer(userName);
            if (player != null)
            {
                Player playerOpponent;
                var game = GameState.Instance.FindGame(player, out playerOpponent);
                if (game != null)
                {
                    if (!string.IsNullOrEmpty(game.WhosTurn) && game.WhosTurn != player.Id)
                    {
                        return true;
                    }

                    var card = FindCard(game, cardName);
                    Clients.Group(player.group).flipCard(card);
                    return true;
                }
            }
            return false;
        }

        private Card FindCard(Game game, string cardName)
        {
            return game.Board.Pieces.FirstOrDefault(c => c.Name == cardName);
        }

        public bool CheckCard(string cardName)
        {
            var userName = Clients.Caller.name;
            Player player = GameState.Instance.GetPlayer(userName);
            if (player != null)
            {
                Player playerOpponent;
                Game game = GameState.Instance.FindGame(player, out playerOpponent);
                if (game != null)
                {
                    if (!string.IsNullOrEmpty(game.WhosTurn) && game.WhosTurn != player.Id)
                        return true;

                    Card card = FindCard(game, cardName);

                    if (game.LastCard == null)
                    {
                        game.WhosTurn = player.Id;
                        game.LastCard = card;
                        return true;
                    }

                    //second flip

                    bool isMatch = IsMatch(game, card);
                    if (isMatch)
                    {
                        StoreMatch(player, card);
                        game.LastCard = null;
                        Clients.Group(player.Group).showMatch(card, userName);

                        if (player.Matches.Count >= 16)
                        {
                            Clients.Group(player.Group).winner(card, userName);
                            GameState.Instance.ResetGame(game);
                            return true;
                        }

                        return true;
                    }

                    Player opponent = GameState.Instance.GetOpponent(player, game);
                    //shift to other player
                    game.WhosTurn = opponent.Id;

                    Clients.Group(player.Group).resetFlip(game.LastCard, card);
                    game.LastCard = null;
                    return true;
                }
            }

            return false;
        }

        private void StoreMatch(Player player, Card card)
        {
            player.Matches.Add(card.Id);
            player.Matches.Add(card.Pair);
        }

        private bool IsMatch(Game game, Card card)
        {
            if (card == null)
                return false;

            if (game.LastCard != null)
            {
                if (game.LastCard.Pair == card.Id)
                {
                    return true;
                }

                return false;
            }

            return false;
        }
    }

    internal class ChatMessages
    {
        public string message { get; set; }
        public string room { get; set; }
    }

    public class ServerInstructions
    {
        public string function { get; set; }
        public string element { get; set; }
        public string arg1 { get; set; }
    }
    public class NewTurnInstructions
    {
        public string title { get; set; }
        public string message { get; set; }
        public List<Buttons> buttons { get; set; }
    }

    public class Buttons
    {
        public string cssClass { get; set; }
        public string label { get; set; }
        public string type { get; set; }

    }
}