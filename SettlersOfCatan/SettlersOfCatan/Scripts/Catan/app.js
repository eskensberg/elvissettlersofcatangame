﻿// - - - hiding the tiles - - -

$(function () {
    var mylist = [
        "tile-1", "tile-2", "tile-3", "tile-4", "tile-5", "tile-6", "tile-7", "tile-8", "tile-9", "tile-13", "tile-14", "tile-15", "tile-16", "tile-21", "tile-22", "tile-28", "tile-28", "tile-29", "tile-30", "tile-35", "tile-36", "tile-37", "tile-41", "tile-42", "tile-43", "tile-44", "tile-45", "tile-46", "tile-47", "tile-48", "tile-49",
        "tile-50", "tile-51", "tile-52", "tile-53", "tile-54", "tile-55"
    ];

    for (var i = 0; i < mylist.length; i++) {
        var myNode = document.getElementById(mylist[i]);
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }
    }
    //myFunction1($('url(http://localhost:11111/Games/Catan/Resources/ship.JPG)'));

    // a function to change background
    function changeImage(param, str) {
        $($(param)).css('background-image', str);
    }

    // and array of id's and results
    var items = [
              //['#shore-0', "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjxeFLMUmLGEaf6NstxXfwgcwCPCkk_tnBtOuEYZXtlTu7V72P)"],
              //['#shore-1', "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjxeFLMUmLGEaf6NstxXfwgcwCPCkk_tnBtOuEYZXtlTu7V72P)"],
              //['#shore-2', "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjxeFLMUmLGEaf6NstxXfwgcwCPCkk_tnBtOuEYZXtlTu7V72P)"],
              //['#shore-3', "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjxeFLMUmLGEaf6NstxXfwgcwCPCkk_tnBtOuEYZXtlTu7V72P)"],
              //['#shore-4', "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjxeFLMUmLGEaf6NstxXfwgcwCPCkk_tnBtOuEYZXtlTu7V72P)"],
              ['#shore-5', "url(/Resources/ship.JPG)"],
              ['#shore-6', "url(/Resources/ship.JPG)"],
              ['#shore-7', "url(/Resources/ship.JPG)"],
              ['#shore-8', "url(/Resources/ship.JPG)"]
    ];

    for (var i = 0; i < items.length; i++) {
        changeImage(items[i][0], items[i][1]);
    }

});