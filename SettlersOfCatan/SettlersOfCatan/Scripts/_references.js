﻿/// <autosync enabled="true" />
/// <reference path="../content/js/functions.js" />
/// <reference path="../resources/js/chathub.js" />
/// <reference path="../resources/js/settlersr.js" />
/// <reference path="../resources/js/test.js" />
/// <reference path="bootstrap.js" />
/// <reference path="bootstrap-dialog.js" />
/// <reference path="catan/app.js" />
/// <reference path="gamefunctiondefinitions.js" />
/// <reference path="handlebars.amd.min.js" />
/// <reference path="handlebars.min.js" />
/// <reference path="handlebars.runtime.amd.min.js" />
/// <reference path="handlebars.runtime.min.js" />
/// <reference path="jquery.signalr-2.2.2.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-3.1.1.js" />
/// <reference path="jquery-3.1.1.slim.min.js" />
/// <reference path="jquery-ui-1.12.1.js" />
/// <reference path="jquery-ui-1.12.1.min.js" />
/// <reference path="knockout-3.4.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
