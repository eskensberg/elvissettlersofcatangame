﻿var pContainerHeight;
var $device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
$(function () {
    pContainerHeight = $('#container-background').height();
    console.log('height: ' + pContainerHeight);
});

//function returnPolygonClipPathValues(ar11,ar12,arg21,arg22,arg3,arg3,arg4,arg4) {
//    return 'polygon(' + (100 - 71 * scrollPercent)
//        + '% 0, ' + (80 + 20 * scrollPercent) + '% 0px, 100% ' + (0 + 100 * scrollPercent)
//        + '%, 0px ' + (100 - 30 * scrollPercent) + '%)';
//}

$(window).scroll(function () {

    var position = $(this).scrollTop();
    var wScroll = position;
    var initialValue = 900;
    var distance = 500;            // =(distance-ABS(initial-MIN(wscroll,initial+distance)))/distance

    if (position >= initialValue && position <= initialValue + 300) {
        wScroll = initialValue;
    } else if (position > initialValue + 300) {
        wScroll = position - 300;
    }
    var scrollPercent = (distance - Math.abs(initialValue - Math.min(wScroll, initialValue + distance))) / distance;

    var clipImagePolygonValues = 'polygon(' + (100 - 71 * scrollPercent)
        + '% 0, ' + (100 - 20 * scrollPercent) + '% 0px, 100% ' + (0 + 100 * scrollPercent) + '%, ' +
        '0px ' + (70) + '%)';

   
    $('.clip-image-right-top').css({
        //'transform': 'translate(0px, ' + containerMoveSize + '%)',
        'clip-path': clipImagePolygonValues  // '+   100 +'% 0,  80% 0px, 100% 0%, 0px 100%
    });                                                                       // '+   29 +'% 0, 100% 0,   100% 100%, 0  70%


    wScroll = position;
    initialValue = 1200;
    if (position >= initialValue && position <= initialValue + 150) {
        wScroll = initialValue;
    } else if (position > initialValue + 150) {
        wScroll = position - 150;
    }
    scrollPercent = (distance - Math.abs(initialValue - Math.min(wScroll, initialValue + distance))) / distance;
    clipImagePolygonValues = 'polygon(-10px 0, ' + (0 + 80 * scrollPercent) + '% 20%, '
        + (0 + 100 * scrollPercent) + '% ' + (100 - 20 * scrollPercent) + '%, 0 100%)';
    // clip-path: polygon(-10px 0, 80% 20%, 100% 80%, 0 100%);
    $('.clip-image-left-top').css({
        //'transform': 'translate(0px, ' + containerMoveSize + '%)',
        'clip-path': clipImagePolygonValues  // '+   100% 0,  80% 0px, 100% 0%, 0px 100%
    });

    wScroll = position;
    initialValue = 1500;
    if (position >= initialValue && position <= initialValue +300) {
        wScroll = initialValue;
    } else if (position > initialValue +300) {
        wScroll = position - 300;
    }
    scrollPercent = (distance - Math.abs(initialValue - Math.min(wScroll, initialValue + distance))) / distance;
    clipImagePolygonValues = 'polygon( 24% 10%, ' +
                                (24 + 76 * scrollPercent) + '% ' + (24 * scrollPercent) + '%, ' +
                                (24 + 76 * scrollPercent) + '% 80%,  ' + (24 + 21 * scrollPercent) + '% ' + (54 + 46 * scrollPercent) + '%, ' +
                                (24 - 24 * scrollPercent) + '% 54%, ' +
                                (24 - 24 * scrollPercent) + '% 15%)';
    $('.clip-image-right-middle').css({
        'clip-path': clipImagePolygonValues   // 24% 10%, 100% 20%, 100% 80%,  45% 100%, 0 54%, 0 15%
    });

    //initialValue = 1600;
    distance = 600;
    scrollPercent = (distance - Math.abs(initialValue - Math.min(wScroll, initialValue + distance))) / distance;
    clipImagePolygonValues = 'polygon(' +
        '0 0, ' +
        (75*scrollPercent) +'% '+ (10*scrollPercent)+'%, ' +
        '' + (90 + 10 * scrollPercent) + '% ' + (80 - 45 * scrollPercent) + '% , ' +
        '90% 80%, ' +
        (90 - 70 * scrollPercent) + '% ' + (80 + 20 * scrollPercent) + '%';
    $('.clip-image-left-middle').css({
        'clip-path': clipImagePolygonValues   // 0 0, 75% 10%, 100% 35% , 90% 80%, 20% 100%
    });


    // ReSharper restore InvalidValue

    // Landing Elements
    //if (wScroll > $('.clothes-pics').offset().top - ($(window).height() / 1.2)) {

    //    $('.clothes-pics figure').each(function (i) {

    //        setTimeout(function () {
    //            $('.clothes-pics figure').eq(i).addClass('is-showing');
    //        }, (700 * (Math.exp(i * 0.14))) - 700);
    //    });

    //}


    // Promoscope
    //if (wScroll > $('.large-window').offset().top - $(window).height()) {

    //    $('.large-window').css({ 'background-position': 'center ' + (wScroll - $('.large-window').offset().top) + 'px' });

    //    var opacity = (wScroll - $('.large-window').offset().top + 400) / (wScroll / 5);

    //    $('.window-tint').css({ 'opacity': opacity });

    //}


    // Floating Elements

    //if (wScroll > $('.blog-posts').offset().top - $(window).height()) {

    //    var offset = (Math.min(0, wScroll - $('.blog-posts').offset().top + $(window).height() - 350)).toFixed();

    //    $('.post-1').css({ 'transform': 'translate(' + offset + 'px, ' + Math.abs(offset * 0.2) + 'px)' });

    //    $('.post-3').css({ 'transform': 'translate(' + Math.abs(offset) + 'px, ' + Math.abs(offset * 0.2) + 'px)' });

    //}
});
