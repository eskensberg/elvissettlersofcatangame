﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SettlersOfCatan.Controllers
{
    public class GoMController : Controller
    {
        // GET: GoM
        public ActionResult Index()
        {
            return View();
        }

        // GET: GoM/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: GoM/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GoM/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: GoM/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: GoM/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: GoM/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: GoM/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
