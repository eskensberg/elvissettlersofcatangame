﻿using System.Linq;
using System.Web.Mvc;

namespace SettlersOfCatan.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Chat()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Music()
        {
            return View();
        }

        public ActionResult Player()
        {
            return View();
        }

        public ActionResult Artists()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Test()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}