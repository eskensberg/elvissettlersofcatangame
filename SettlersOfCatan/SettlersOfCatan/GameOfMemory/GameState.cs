﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using SettlersOfCatan.GameOfMemory.Models;
using SettlersOfCatan.Hubs;

namespace SettlersOfCatan.GameOfMemory
{
    public class GameState
    {
        private static readonly Lazy<GameState> _instance = new Lazy<GameState>( 
            () => new GameState(GlobalHost.ConnectionManager.GetHubContext<PerfHub>()));

        private readonly ConcurrentDictionary<string, Player> _players =
            new ConcurrentDictionary<string, Player>(StringComparer.OrdinalIgnoreCase);

        private readonly ConcurrentDictionary<string, Game> _games =
            new ConcurrentDictionary<string, Game>(StringComparer.OrdinalIgnoreCase);

        private readonly ConcurrentDictionary<string, SettlersGame> _settlergames =
            new ConcurrentDictionary<string, SettlersGame>(StringComparer.OrdinalIgnoreCase);

        public GameState(IHubContext context)
        {
            Clients = context.Clients;
            Groups = context.Groups;
        }

        public static GameState Instance
        {
            get { return _instance.Value; }
        }

        // TODO: Could be string or something else instead of dynamic
        public IHubConnectionContext<dynamic> Clients { get; set; }
        public IGroupManager Groups { get; set; }

        public Player CreatePlayer(string userName)
        {
            var player = new Player(userName, GetMD5Hash(userName));
            _players[userName] = player;
            return player;
        }

        private string GetMD5Hash(string userName)
        {
            return String.Join("", MD5.Create()
                         .ComputeHash(Encoding.Default.GetBytes(userName))
                         .Select(b => b.ToString("x2")));
        }

        public Player GetPlayer(string userName)
        {
            return _players.Values.FirstOrDefault(u => u.Name == userName);
        }

        public Player GetNewOpponent(Player player)
        {
            return _players.Values.FirstOrDefault((u => !u.IsPlaying && u.Id != player.Id));
        }

        public Player GetOpponent(Player player, Game game)
        {
            if (game.Player1.Id == player.Id)
                return game.Player2;
            return game.Player1;
        }

        public Game CreateGame(Player player1, Player player2)
        {
            var game = new Game()
            {
                Player1 = player1,
                Player2 = player2,
                Board = new Board()
            };

            var group = Guid.NewGuid().ToString("d");
            _games[group] = game;

            player1.IsPlaying = true;
            player1.Group = group;

            player2.IsPlaying = true;
            player2.Group = group;

            Groups.Add(player1.ConnectionId, group);
            Groups.Add(player2.ConnectionId, group);

            return game;
        }

        public SettlersGame CreateSettlersGame(Player player1, Player player2, Player player3, Player player4)
        {
            var game = new SettlersGame()
            {
                Player1 = player1,
                Player2 = player2,
                Player3 = player3,
                Player4 = player4,
                Board = new SettlersBoard()
            };

            var group = Guid.NewGuid().ToString("d");
            _settlergames[group] = game;

            player1.IsPlaying = true;
            player1.Group = group;

            player2.IsPlaying = true;
            player2.Group = group;

            player3.IsPlaying = true;
            player3.Group = group;

            player4.IsPlaying = true;
            player4.Group = group;

            Groups.Add(player1.ConnectionId, group);
            Groups.Add(player2.ConnectionId, group);
            Groups.Add(player3.ConnectionId, group);
            Groups.Add(player4.ConnectionId, group);

            return game;
        }

        public Game FindGame(Player player, out Player opponent)
        {
            opponent = null;
            if (player.Group == null)
                return null;

            Game game;
            _games.TryGetValue(player.Group, out game);

            if (game != null)
            {
                if (player.Id == game.Player1.Id)
                {
                    opponent = game.Player2;
                    return game;
                }

                opponent = game.Player1;
                return game;

            }
            return null;
        }

        public SettlersGame FindSettlersGame(Player player1, out Player opponent2, out Player opponent3, out Player opponent4)
        {
            opponent2 = null;
            opponent3 = null;
            opponent4 = null;
            if (player1.Group == null)
                return null;

            SettlersGame game;
            _settlergames.TryGetValue(player1.Group, out game);

            if (game != null)
            {   // if player one, then openents are other players
                if (player1.Id == game.Player1.Id)
                {   
                    // chain to find or set the rest of the players
                    //opponent2 = game.Player2;
                    return game;
                }

                opponent2 = game.Player1;
                return game;

            }
            return null;
        }

        public void ResetGame(Game game)
        {
            var groupName = game.Player1.Group;
            var player1Name = game.Player1.Name;
            var player2Name = game.Player2.Name;

            Groups.Remove(game.Player1.ConnectionId, groupName);
            Groups.Remove(game.Player2.ConnectionId, groupName);

            Player p1;
            _players.TryRemove(player1Name, out p1);

            Player p2;
            _players.TryRemove(player2Name, out p2);

            Game g;
            _games.TryRemove(groupName, out g);


        }
   
        public void ResetSettlersGame(SettlersGame game)
        {
            var groupName = game.Player1.Group;
            var player1Name = game.Player1.Name;
            var player2Name = game.Player2.Name;
            var player3Name = game.Player3.Name;
            var player4Name = game.Player4.Name;

            Groups.Remove(game.Player1.ConnectionId, groupName);
            Groups.Remove(game.Player2.ConnectionId, groupName);
            Groups.Remove(game.Player3.ConnectionId, groupName);
            Groups.Remove(game.Player4.ConnectionId, groupName);

            Player p1;
            _players.TryRemove(player1Name, out p1);

            Player p2;
            _players.TryRemove(player2Name, out p2);

            Player p3;
            _players.TryRemove(player3Name, out p3);

            Player p4;
            _players.TryRemove(player4Name, out p4);

            SettlersGame g;
            _settlergames.TryRemove(groupName, out g);


        }
    }
}