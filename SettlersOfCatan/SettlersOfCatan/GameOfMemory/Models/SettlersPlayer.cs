using System;
using System.Collections.Generic;

namespace SettlersOfCatan.GameOfMemory.Models
{
    public class SettlersPlayer
    {
        public SettlersPlayer(string name, string hash)
        {
            Name = name;
            Hash = hash;
            Id = Guid.NewGuid().ToString("d");
            Points = new List<int>();
        }

        public string ConnectionId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Hash { get; set; }
        public string Group { get; set; }

        public bool IsPlaying { get; set; }

        public List<int> Points { get; set; }

        public List<int> Deck { get; set; } // for
    }
}