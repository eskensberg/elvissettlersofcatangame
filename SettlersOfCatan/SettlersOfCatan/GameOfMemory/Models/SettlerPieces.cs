﻿namespace SettlersOfCatan.GameOfMemory.Models
{
    public class Tiles
    {
        public int Id { get; set; }
        public string Type { get; set; } // desert, wheat etc
        public string DiceValue { get; set; } // desert, wheat etc
    }
}