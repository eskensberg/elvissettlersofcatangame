namespace SettlersOfCatan.GameOfMemory.Models
{
    public class SettlersGame
    {
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
        public Player Player3 { get; set; }
        public Player Player4 { get; set; }

        public SettlersBoard Board { get; set; }
        public string WhosTurn { get; set; }
    }
}