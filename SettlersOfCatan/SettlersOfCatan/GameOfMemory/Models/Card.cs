﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SettlersOfCatan.GameOfMemory.Models
{
    public class Card
    {
        public int Id { get; set; }
        public int Pair { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }

    public class Harbor
    {
        public int Id { get; set; }
        public string Type { get; set; } // desert, wheat etc
    }
    public class House
    {
        public string Id { get; set; }
        public string Owner { get; set; }  // player owning it
        public string border1 { get; set; } // desert, wheat etc
        public string border2 { get; set; } // desert, wheat etc
        public string border3 { get; set; } // desert, wheat etc
    }
    public class Road
    {
        public string Id { get; set; }
        public string Owner { get; set; }  // player owning it
        public string adjacentHouse1 { get; set; } // desert, wheat etc
        public string adjacentHouse2 { get; set; } // desert, wheat etc
        public string border1 { get; set; } // desert, wheat etc
        public string border2 { get; set; } // desert, wheat etc
    }
}