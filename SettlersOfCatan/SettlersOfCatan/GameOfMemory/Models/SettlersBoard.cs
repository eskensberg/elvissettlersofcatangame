using System;
using System.Collections.Generic;
using System.Linq;

namespace SettlersOfCatan.GameOfMemory.Models
{
    public class SettlersBoard
    {
        public List<Tiles> Tiles { get; set; } = new List<Tiles>();
        public List<Harbor> Harbor { get; set; } = new List<Harbor>();
        public List<House> House { get; set; } = new List<House>();
        public List<Road> Road { get; set; } = new List<Road>();

        public string Room { get; set; }
        public string Player1 { get; set; }
        public string Player1Colour { get; set; } = "red";
        public string Player2 { get; set; }
        public string Player2Colour { get; set; } = "blue";
        public string Player3 { get; set; }
        public string Player3Colour { get; set; } = "white";
        public string Player4 { get; set; }
        public string Player4Colour { get; set; } = "orange";
        public string WhosTurn { get; set; }
        public int[] Edges { get; set; }


        //  "tile-1", "tile-2", "tile-3", "tile-4", "tile-5", "tile-6", "tile-7", "tile-8", "tile-9", "tile-13", "tile-14", "tile-15", "tile-16", "tile-21", "tile-22", "tile-28", "tile-28", "tile-29", "tile-30", "tile-35", "tile-36", "tile-37", "tile-41", "tile-42", "tile-43", "tile-44", "tile-45", "tile-46", "tile-47", "tile-48", "tile-49",
        //"tile-50", "tile-51", "tile-52", "tile-53", "tile-54", "tile-55"

        public SettlersBoard()
        {
            //  Dice Values                     5   2   6   10  9   4   3   8   11      5   8   4   3   6   10  11  12  9                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
            //                                  A   B   C   L   M   N   D   K   R   XX  O   E   J   Q   P   F   I   H   G
            var ListOfTileIDs = new List<int> { 10, 11, 12, 17, 18, 19, 20, 23, 24, 25, 26, 27, 31, 32, 33, 34, 38, 39, 40  };
            //var OriginalValues = new List<int> { 10, 11, 12, 17, 18, 19, 20, 23, 24, 25, 26, 27, 31, 32, 33, 34, 38, 39, 40 };

            ListOfTileIDs.Shuffle();

            var ListOfHarborIDs = new List<int>(Enumerable.Range(0, 9));
            //ListOfHarborIDs.AddRange(Enumerable.Range(0, 9));
            ListOfHarborIDs.Shuffle();

            var DiceValues = new List<int> { 5, 2, 6, 10, 9, 4, 3, 8, 11, 5, 8, 4, 3, 6, 10, 11, 12, 9 };
            //ListOfTileIDs.AddRange(OriginalValues);
            //ListOfTileIDs.AddRange(DiceValues);
            //int desertValue = ListOfTileIDs.First();
            //OriginalValues.RemoveAt(desertValue);
            //OriginalValues.AddRange(DiceValues);
            //ListOfTileIDs.AddRange(OriginalValues);

            #region Create Tiles
            int i = 1;
            foreach (var tile in ListOfTileIDs)
            {
                var type = String.Empty;

                // 1 desert, 4 wheet/sheep/wood, 3 ore/brick
                if (i == 1)
                {
                    type = "Desert";
                }
                else if (i >= 2 && i <= 5) // 2 to 5 wheet
                {
                    type = "Wheat";
                }
                else if (i >= 6 && i <= 9) //6 to 9
                {
                    type = "Sheep";
                }
                else if (i >= 10 && i <= 13) // 10 to 13
                {
                    type = "Wood";
                }
                else if (i >= 14 && i <= 16) // 14 to 16
                {
                    type = "Ore";
                }
                else if (i >= 17 && i <= 19) // 17 to 19
                {
                    type = "Brick";
                }
                else
                {
                    type = "";
                }
                Tiles.Add(new Tiles()
                {
                    Id = tile,
                    Type = type
                });
                i++;
            }
            #endregion


            #region Create Harbors
            i = 1;
            foreach (var tile in ListOfHarborIDs)
            {
                var type = String.Empty;

                // 1 desert, 4 wheet/sheep/wood, 3 ore/brick
                if (i == 1)
                {
                    type = "Wheet_Factory";
                }
                else if (i == 2)
                {
                    type = "Sheep_Factory";
                }
                else if (i == 3)
                {
                    type = "Wood_Factory";
                }
                else if (i == 4)
                {
                    type = "Ore_Factory";
                }
                else if (i == 5)
                {
                    type = "Brick_Factory";
                }
                else
                {
                    type = "341";
                }

                Harbor.Add(new Harbor()
                {
                    Id = tile,
                    Type = type
                });
                i++;
            }
            #endregion


        }
    }
}